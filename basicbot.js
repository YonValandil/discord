const fs = require('fs')
const discord = require('discord.js')
const { token } = require('./config.json')

// 2 features not working (doc) :
//  The avatar's image
//  the aliases names for the commands
// Kick command doesn't kick user yet

const client = new discord.Client()

const eventFiles = fs.readdirSync('./events').filter(file => file.endsWith('.js'))
for (const file of eventFiles) {
  const event = require(`./events/${file}`)
  if (event.once) {
    client.once(event.name, (...args) => event.execute(...args, client))
  } else {
    client.on(event.name, (...args) => event.execute(...args, client))
  }
}

client.login(token)
