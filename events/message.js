const { prefix } = require('./../config.json')
const discord = require('./../node_modules/discord.js')
const fs = require('fs')

module.exports = {
  name: 'message',
  execute (message, client) {
    if (!message.content.startsWith(prefix) || message.author.bot) return

    console.log(`${message.author.tag} in #${message.channel.name} sent: ${message.content}`)

    client.commands = new discord.Collection()
    client.cooldowns = new discord.Collection()

    const commandFolders = fs.readdirSync('./commands')
    for (const folder of commandFolders) {
      const commandFiles = fs.readdirSync(`./commands/${folder}`).filter(file => file.endsWith('.js'))
      for (const file of commandFiles) {
        const command = require(`./../commands/${folder}/${file}`)
        client.commands.set(command.name, command)
      }
    }

    const args = message.content.slice(prefix.length).trim().split(/ +/)
    const commandName = args.shift().toLowerCase()

    if (!client.commands.has(commandName)) return

    // Get the command + the aliases (not working)
    const command = client.commands.get(commandName) ||
  client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName))

    // Option guildOnly: For commands only available on server (not direct message)
    if (command.guildOnly && message.channel.type === 'dm') {
      return message.reply('I can\'t execute that command inside DMs!')
    }

    // Option Permissions: Check permissions
    if (command.permissions) {
      const authorPerms = message.channel.permissionsFor(message.author)
      if (!authorPerms || !authorPerms.has(command.permissions)) {
        return message.reply('You can not do this!')
      }
    }

    // Option args: For commands with mandatory args + optional usage
    if (command.args && !args.length) {
      let reply = `You didn't provide any arguments, ${message.author}!`
      if (command.usage) {
        reply += `\nThe proper usage would be: \`${prefix}${command.name} ${command.usage}\``
      }
      return message.channel.send(reply)
    }

    // Option cooldowns: Add a cooldown, cooldowns > command > user > timestamp (Ex: Against spam)
    const { cooldowns } = client
    if (!cooldowns.has(command.name)) {
      cooldowns.set(command.name, new discord.Collection())
    }

    const now = Date.now()
    const timestamps = cooldowns.get(command.name)
    const cooldownAmount = (command.cooldown || 3) * 1000

    if (timestamps.has(message.author.id)) {
      const expirationTime = timestamps.get(message.author.id) + cooldownAmount

      if (now < expirationTime) {
        const timeLeft = (expirationTime - now) / 1000
        return message.reply(`please wait ${timeLeft.toFixed(1)} more second(s) before reusing the \`${command.name}\` command.`)
      }
    }
    timestamps.set(message.author.id, now)
    setTimeout(() => timestamps.delete(message.author.id), cooldownAmount)

    // Execution
    try {
      command.execute(message, args)
    } catch (error) {
      console.error(error)
      message.reply('there was an error trying to execute that command!')
    }
  }
}
