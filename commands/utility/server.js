module.exports = {
  name: 'server',
  description: 'Display info about this server.',
  execute (message) {
    message.channel.send(
      `
      Server name: ${message.guild.name}
      Total members: ${message.guild.memberCount}
      Region: ${message.guild.region}
      Time: ${message.guild.createdAt}
      `
    )
  }
}
